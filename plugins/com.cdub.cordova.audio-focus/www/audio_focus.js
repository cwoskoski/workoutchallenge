
var AudioFocus = function(){

};

AudioFocus.prototype = {
    request: function(){
        cordova.exec(null, null, 'AudioFocus', 'request', []);
    },
    release: function(){
        cordova.exec(null, null, 'AudioFocus', 'release', []);
    }
};

var plugin  = new AudioFocus(),
    channel = require('cordova/channel');

channel.deviceready.subscribe( function () {
    cordova.exec(null, null, 'AudioFocus', 'deviceready', []);
});

module.exports = plugin;
