module.exports = {
    dist: {                            // Target
      options: {                       // Target options
        style: 'compressed'
      },
      files: {                         // Dictionary of files
        'www/css/style.min.css': 'www/css/style.scss'     // 'destination': 'source'
      }
    }
  }