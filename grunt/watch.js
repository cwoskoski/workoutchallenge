module.exports = {
    css: {
        files: '**/*.scss',
        tasks: ['sass']
    },
    package: {
        files: 'www/config.xml',
        tasks: ['copy-part-of-file:copy_config']
    }
}