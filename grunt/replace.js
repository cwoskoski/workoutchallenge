module.exports = function (grunt, options) {

    return {
        debug: {
            src: [
                'platforms/android/AndroidManifest.xml'
            ],
            overwrite: true,                 // overwrite matched source files
            replacements: [
                {
                    from: /android:debuggable=".*?"/g,
                    to: "android:debuggable=\"true\""
                }
            ]
        },release_manifest: {
            src: [
                'platforms/android/AndroidManifest.xml'
            ],
            overwrite: true,                 // overwrite matched source files
            replacements: [
                {
                    from: /android:debuggable=".*?"/g,
                    to: "android:debuggable=\"false\""
                }
            ]
        },
        reset_configs: {
            src: [
                'config.xml',
                'www/config.xml'
            ],
            overwrite: true,                 // overwrite matched source files
            replacements: [
                {
                    from: /version=".*?"/g,
                    to: function (match, val, all, matchArr) {
                        return 'version="' + grunt.config('pkg.version') + '"';
                    }
                },
                {
                    from: /android-versionCode=".*?"/g,
                    to: function (match, val, all, matchArr) {
                        return 'android-versionCode="' + grunt.config('pkg.version_code') + '"';
                    }
                },
                {
                    from: /widget id=".*?"/g,
                    to: function (match, val, all, matchArr) {
                        return 'widget id="'+ options.package.package_name +'"';
                    }
                },
                {
                    from: /\<name\>.*?\<\/name\>/g,
                    to: function () {
                        return '<name>'+ options.package.description +'</name>';
                    }
                }
            ]
        },
        change_to_beta: {
            src: [
                'www/config.xml',
                'config.xml'
            ],
            overwrite: true,                 // overwrite matched source files
            replacements: [
                {
                    from: /widget id=".*?"/g,
                    to: function (match, val, all, matchArr) {
                        return 'widget id="'+ options.package.package_name +'.beta"';
                    }
                },
                {
                    from: /\<name\>.*?\<\/name\>/g,
                    to: function () {
                        return '<name>'+ options.package.description +' BETA</name>';
                    }
                }
            ]
        }
    }
};