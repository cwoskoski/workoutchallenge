module.exports = function(grunt, options){

    var VERSIONCODE_REGEXP = /([\'|\"]?version_code[\'|\"]?[ ]*:[ ]*[\'|\"]?)([\d||A-a|.|-]*)([\'|\"]?)/i;

    grunt.registerTask('bump_versioncode', 'Increment the android version code', function() {
        var file = 'package.json',
            version,
            content = grunt.file.read(file).replace(VERSIONCODE_REGEXP, function(match, prefix, parsedVersionCode, suffix) {
                version = parseInt(parsedVersionCode) + 1;
                return prefix + version + suffix;;
            });

        if (!version) {
            grunt.fatal('Can not find a version to bump in ' + file);
        }

        grunt.config("pkg.version_code", version);
        grunt.file.write(file, content);

        grunt.log.ok('version_code\'s version updated to '+ version);
    });
};
