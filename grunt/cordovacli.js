module.exports = {
    options: {
        path: 'www'
    },
    build: {
        options: {
            command: 'build',
            platforms: ['android']
        }
    },
    release: {
        options: {
            command: 'build',
            platforms: ['android'],
            args: ['--release']
        }
    },
    run_phone: {
        options: {
            command: 'run',
            platforms: ['android'],
            args: ['--target=LGD850a2e1af00']
        }
    }
};