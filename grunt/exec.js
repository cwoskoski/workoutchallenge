module.exports = {
    rm_signed_apk: {
        command: 'rm -f platforms\\android\\ant-build\\WorkoutChallenge-release-signed.apk',
        stdout: true,
        stderr: true
    },
    rm_aligned_apk: {
        command: 'rm -f platforms\\android\\ant-build\\WorkoutChallenge-release-signed-aligned.apk',
        stdout: true,
        stderr: true
    },
    sign_apk: {
        command: 'jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -storepass Catalina1 -keystore resources\\android\\wc.keystore platforms\\android\\ant-build\\WorkoutChallenge-release-unsigned.apk workout_challenge',
        stdout: true,
        stderr: true
    },
    move_signed_apk: {
        command: 'mv platforms\\android\\ant-build\\WorkoutChallenge-release-unsigned.apk platforms\\android\\ant-build\\WorkoutChallenge-release-signed.apk',
        stdout: true,
        stderr: true
    },
    zipalign_apk: {
        command: 'F:\\Devel\\android-sdk-r20\\build-tools\\19.1.0\\zipalign -v 4 platforms\\android\\ant-build\\WorkoutChallenge-release-signed.apk platforms\\android\\ant-build\\WorkoutChallenge-release-signed-aligned.apk',
        stdout: true,
        stderr: true
    },
    open_apk_folder: {
        command: 'start platforms\\android\\ant-build',
        stdout: true,
        stderr: true
    }
};