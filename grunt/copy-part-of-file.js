module.exports = {
    copy_config: {
        options: {
            sourceFileStartPattern: '<!-- SIMPLE START -->',
            sourceFileEndPattern: '<!-- SIMPLE END -->',
            destinationFileStartPattern: '<!-- SIMPLE START -->',
            destinationFileEndPattern: '<!-- SIMPLE END -->'
        },
        files: {
            'config.xml': ['www/config.xml']
        }
    }
}