module.exports = {
    default: [],
    sign: [
        'exec:rm_signed_apk',
        'exec:rm_aligned_apk',
        'exec:sign_apk',
        'exec:move_signed_apk',
        'exec:zipalign_apk',
        'exec:rm_signed_apk'
    ],
    debugPhone: [
        'replace:reset_configs',
        'replace:change_to_beta',
        'replace:debug',
        'cordovacli:run_phone'
    ],
    beta: [
        'prompt',
        'bump',
        'bump_versioncode',
        'replace:reset_configs',
        'replace:change_to_beta',
        'cordovacli:release',
        'sign',
        "exec:open_apk_folder"
    ],
    live: [
        'prompt',
        'bump',
        'bump_versioncode',
        'replace:reset_configs',
        'cordovacli:release',
        'sign',
        "exec:open_apk_folder"
    ]
};