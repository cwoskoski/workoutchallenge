module.exports = function (grunt, options) {

    return {
        options: {
            files: ['package.json'],
            updateConfigs: ['pkg'],
            commit: false,
            commitMessage: 'Release v%VERSION%',
            commitFiles: [],
            createTag: false,
            tagName: 'v%VERSION%',
            tagMessage: 'Version %VERSION%',
            push: false,
            pushTo: 'upstream',
            gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
            globalReplace: false
        }
    }
};