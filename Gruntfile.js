module.exports = function(grunt, options) {

	require('time-grunt')(grunt);
	require('load-grunt-config')(grunt, {
        data: {
            pkg: grunt.file.readJSON('package.json')
        }
    });
};