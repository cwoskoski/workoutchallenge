/**
 * Created by Mike on 2/26/14.
 */

define([
    'jquery',
    'underscore',
    'backbone',

    'views/header',
    'views/preview',
    'views/workout_list',
    'views/workout',
    'views/workout_details',
    'views/tts',

    'collections/workouts',
    'collections/exercises'

], function(
    $, _, Backbone,
    HeaderView, PreviewView, WorkoutListView, WorkoutView, WorkoutDetails, TtsView,
    WorkoutsCollection, ExercisesCollection
    ){

    var MainRouter = Backbone.Router.extend({
        activeView: false,
        initialize: function(){

            window.headerView = new HeaderView();

            this.workoutsCollection = new WorkoutsCollection();
            this.workoutsCollection.checkServer();

            this.exercisesCollection = new ExercisesCollection();
            this.exercisesCollection.checkServer();
        },

        routes: {
            "" : "defaultRoute",
            "workout/:name": "workout",
            "details/:name": "details",
            "tts": "tts"
        },

        defaultRoute: function(){

            var self = this;
            var view;
            if(!Backbone.getAttribute('previewed', false)) {
                view = new PreviewView({collection: this.workoutsCollection, model: this.workoutsCollection.findWhere({name: "All Over My Body"})});
            } else {
                view = new WorkoutListView({collection: this.workoutsCollection});
            }
            self._showView(view);
        },

        workout: function(name){
            var self = this;

            var workouts = this.workoutsCollection;
            var view = new WorkoutView({model: workouts.findWhere({name: name})});
            self._showView(view);
        },

        details: function(name){
            var self = this;

            var workouts = this.workoutsCollection;
            var view = new WorkoutDetails({model: workouts.findWhere({name: name}), exercises: this.exercisesCollection});
            self._showView(view);
        },

        tts: function(){
            var self = this;

            var view = new TtsView();
            self._showView(view);
        },

        _showView: function(v) {
            if(this.activeView)
            {
                this.activeView.close();
                this.activeView.undelegateEvents();
            }

            this.activeView = v;
            this.activeView.open();
        }
    });

    return MainRouter;

});