/**
 * Created by Mike on 2/26/14.
 */

(function() {
    'use strict';

    var root = this; //asdf

    var Rotator = function(el, imgList, delay, onSwitch) {
        var paused = true
            , ndx = 0
            , interval = false
            , tick = function() {
                var preload = $('<img/>');
                preload.load(function(){
                    var pl = $(this);
                    el.fadeOut('slow', function(){
                        $(this).attr('src', pl.attr('src')).fadeIn('slow', function(){
                            ndx++;

                            animateDiv($(this).parent());

                            onSwitch();
                            preload.remove();
                        });
                    });
                });

                preload.attr('src', imgList[ndx % imgList.length]);
            }
            , makeNewPosition = function($container, elW, elH) {

                // Get viewport dimensions (remove the dimension of the div)
                $container = ($container || $(window));
                var h = $container.height() - elH;
                var w = $container.width() - elW;

                var nh = Math.floor(Math.random() * h);
                var nw = Math.floor(Math.random() * w);

                return [nh, nw];

            }
            , animateDiv = function($target) {
                var newq = makeNewPosition($target.parent(), $target.width(), $target.height());

                var oldq = $target.offset();
                var speed = 15000;

                $target.animate({
                    top: newq[0],
                    left: newq[1],
                    width: (Math.floor(Math.random() * 100) + 50) + '%'
                }, speed, function() {
                    if(!paused && interval)
                    {
                        (function(self){
                            return function() { animateDiv($target); };
                        })(this)
                    }
                });

            };

        return {
            start: function(){
                tick.call(self);
                interval = setInterval(
                    (function(self){
                        return function() { tick.call(self); };
                    })(this), delay
                );
                paused = false;
            },

            stop: function(){
                el.hide();
                clearInterval(interval);
                paused = true;
            }
        };
    };

    if (typeof exports !== 'undefined') module.exports = exports = Rotator;
    else root.Rotator = Rotator;

}).call(this);
