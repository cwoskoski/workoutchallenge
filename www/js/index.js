/**
 * Created by Chad on 10/28/13.
 */

require.config({
    paths: {
        'text': '../bower_components/requirejs-text/text',
        async: '../bower_components/async/lib/async',
        jquery: '../bower_components/jquery/dist/jquery.min',
        backbone: '../bower_components/backbone/backbone',
        underscore: '../bower_components/lodash/dist/lodash.compat.min',
        bootstrap: '../js/bootstrap.min',
        bootbox: '../bower_components/bootbox/bootbox',
        moment: '../bower_components/moment/min/moment.min',
        countdown: '../bower_components/countdown.js/lib/countdown',
        rotator: 'image_rotator',
        gtvcore: 'gtv-lib/js/gtvcore',
        gtvkeycontrol: 'gtv-lib/js/keycontrol'
    },
    waitSeconds: 60,
    shim: {
        backbone: {
            deps: [
                'jquery',
                'underscore'
            ],
            exports: 'Backbone'
        },
        underscore: {
            exports: '_'
        },
        bootstrap: {
            deps: [
                'jquery'
            ]
        },
        countdown: {
            exports: 'Countdown'
        },
        rotator: {
            exports: 'Rotator'
        },
        async: {
            exports: 'async'
        },
        gtvcore: {
            deps: [
                'jquery'
            ]
        },
        gtvkeycontrol: {
            deps: [
                'gtvcore'
            ]
        },
        bootbox: {
            deps: [
                'bootstrap'
            ],
            exports: 'bootbox'
        }
    }
});

var app;

require([
    'jquery',
    'backbone',
    'bootstrap',
    'underscore',
    'router',
    'bootbox',
    'text!../config.xml'
], function(jquery, Backbone, Bootstrap, _, Router, bootbox, appConfigXml){

    /*if($.type(console) != 'object')
    {
        var console = {};
        console.log = function(){};
    }*/

    Backbone.setAttribute = function(key, value){
        var s = window.localStorage,
            sAttribs = s.getItem('global-flags') || '';

        sAttribs = (sAttribs.length) ? JSON.parse(sAttribs) : {};
        sAttribs[key] = value;

        s.setItem('global-flags', JSON.stringify(sAttribs));

    };

    Backbone.getAttribute = function(key, defaultVal){
        var s = window.localStorage,
            sAttribs = s.getItem('global-flags') || '';

        sAttribs = (sAttribs.length) ? JSON.parse(sAttribs) : {};

        return (_.has(sAttribs, key)) ? sAttribs[key] : defaultVal;
    };

    Backbone.notification = {
        alert: function(message, cb, title, buttonName){
            if(_.isObject(navigator.notification)){
                navigator.notification.alert(message, cb, title, buttonName);
            } else {
                bootbox.dialog({
                    title: title,
                    message: message,
                    buttons: {
                        main: {
                            label: (buttonName || "Ok"),
                            className: 'btn-primary',
                            callback: function(){
                                bootbox.hideAll();

                                if(cb)
                                    cb();
                            }
                        }
                    }
                })
            }
        },
        confirm: function(message, cb, title, buttonLabels){

            if(_.isObject(navigator.notification)){
                navigator.notification.confirm(message, cb, title, buttonLabels);
            } else {
                var btns = {};
                if(_.isArray(buttonLabels)) _.each(buttonLabels, function(btn){
                    btns[btn] = {
                        label: btn,
                        className: 'btn-default',
                        callback: function(event){
                            var btn = $(event.currentTarget);
                            var footerBtns = btn.parents(".modal-footer").find("button");

                            var btnNdx = 0;
                            footerBtns.each(function(ndx, checkBtn){
                                if(btn.text() == $(checkBtn).text()) {
                                    btnNdx = ndx + 1;
                                    return false;
                                }
                            });

                            bootbox.hideAll();
                            if(cb)
                                cb(btnNdx);
                        }
                    };
                });

                bootbox.dialog({
                    title: title,
                    message: message,
                    buttons: btns
                })
            }
        },
        prompt: function(message, cb, title, buttonLabels, defaultText){
            if(_.isObject(navigator.notification)){
                navigator.notification.prompt(message, cb, title, buttonLabels, defaultText);
            } else {

                var btns = {};
                if(_.isArray(buttonLabels)) _.each(buttonLabels, function(btn){

                    btns[btn] = {
                        label: btn,
                        className: 'btn-default',
                        callback: function(event){
                            var btn = $(event.currentTarget);
                            var footerBtns = btn.parents(".modal-footer").find("button")

                            var btnNdx = 0;
                            footerBtns.each(function(ndx, checkBtn){
                                if(btn.text() == $(checkBtn).text()) {
                                    btnNdx = ndx + 1;
                                    return false;
                                }
                            });
                            var result = {
                                buttonIndex: btnNdx,
                                input1: $(".input-prompt").val()
                            };
                            bootbox.hideAll();

                            if(cb)
                                cb(result);
                        }
                    };
                });

                bootbox.dialog({
                    title: title,
                    message: message + '<div><form class=""><input type="text" class="input-prompt" value="'+(defaultText || '')+'"/></form></div>',
                    buttons: btns
                })
            }
        }
    };

    Backbone.View.prototype.open = function(){
        if (this.onOpen){
            this.onOpen();
        }
    };

    Backbone.View.prototype.close = function(){
        if (this.onClose){
            this.onClose();
        }
    };

    _.mixin({
        'setAttribute': Backbone.setAttribute
    });

    _.mixin({
        'getAttribute': Backbone.getAttribute
    });

    _.mixin({
        'compareVersions': function(v1, comparator, v2) {
            "use strict";
            comparator = comparator == '=' ? '==' : comparator;
            var v1parts = v1.split('.'), v2parts = v2.split('.');
            var maxLen = Math.max(v1parts.length, v2parts.length);
            var part1, part2;
            var cmp = 0;
            for(var i = 0; i < maxLen && !cmp; i++) {
                part1 = parseInt(v1parts[i], 10) || 0;
                part2 = parseInt(v2parts[i], 10) || 0;
                if(part1 < part2)
                    cmp = 1;
                if(part1 > part2)
                    cmp = -1;
            }
            return eval('0' + comparator + cmp);
        }
    });

    _.mixin({
        'get': function(obj, propery) {
            return (_.isUndefined(obj) ? null : _.result(obj, propery));
        }
    });

    _.mixin({
        'ucwords': function(str){
            return (str) ? str.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); }) : '';
        }
    });

    _.mixin({
        'selected': function(val, match){
            return (val == match) ? " selected=\"selected\"" : "";
        }
    });

    var registeredGoBackListeners = {};
    _.mixin({
        'registerGoBack': function(cb){
            var ndx = registeredGoBackListeners.length;
            registeredGoBackListeners[ndx] = cb;
            return ndx;
        }
    });
    _.mixin({
        'unregisterGoBack': function(id){
            if(_.has(registeredGoBackListeners, id)) {
                delete registeredGoBackListeners[id];
                return true;
            }
            return false;
        }
    });

    $(document).on('click', 'a.external', function (event){
        e = event || window.event;
        var t = e.target || e.srcElement
        if ( t.name || t.href ){
            if( typeof t.href == "string" && t.href.substr(0,4) == 'http' ){
                if( t.attributes.href.value !== "#" ){
                    window.open(t.href, '_system', 'location=yes');
                }
                return false; // no further action for this click
            }
        }
        return true; // process click as normal
    })

    try {
        window.appConfig = $($.parseXML(appConfigXml));
        window.isBeta = window.appConfig.find("widget:first").attr('id').match('.beta').length > 0;
    } catch (e) {
        window.appConfig = {};
        window.isBeta = false;
    }

    console.log("beta "+ window.isBeta);

    function onDeviceReady(desktop) {
        // Hiding splash screen when app is loaded
        //if (desktop !== true)
        //    cordova.exec(null, null, 'SplashScreen', 'hide', []);

        try {
            console.log(JSON.stringify({
                available: device.available,
                platform: device.platform,
                version: device.version,
                uuid: device.uuid,
                cordova: device.cordova,
                model: device.model
            }, null, "\n"));

            _.mixin({
                device: function(name){
                    return device ? device[name] : null;
                }
            });
        } catch(e){
            _.mixin({
                device: function(name){
                    return null;
                }
            });
        }

        //todo: prep for ios notification permissions
        /*window.plugin.notification.local.hasPermission(function (granted) {
            console.log('Permission has been granted: ' + granted);
            //window.plugin.notification.local.promptForPermission();
        });*/

        document.addEventListener("backbutton", function (e) {
            var ret,
                refE = e;
            _.each(registeredGoBackListeners, function(cb){
                ret = cb(e);

                console.log("back ret:"+ ret);

                if(ret === false) {
                    refE.preventDefault();
                }
            });
        }, false );

        try {
            app = new Router();
        } catch(e){
            console.log(e.message);
            console.log(e.stack);
        }
        console.log('after route init');
        Backbone.history.start();
    }

    if (!cordovaOnDeviceReadyOverride && navigator.userAgent.match(/(iPad|iPhone|Android)/)) {
        console.log('Should be a phone');
        // This is running on a device so waiting for deviceready event
        document.addEventListener('deviceready', onDeviceReady, false);
    } else {
        // On desktop don't have to wait for anything
        console.log('Should be a regular browser');
        onDeviceReady(true);
    }

});