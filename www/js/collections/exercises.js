/**
 * Created by Mike on 2/26/14.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'models/exercise',
    'text!../exercises.json'
], function ($, _, Backbone, ExerciseModel, ExercisesJson) {
    var ExerciseCollection = Backbone.Collection.extend({
        model: ExerciseModel,
        initialize: function () {
            this.storage = window.localStorage;
            this.storeageKey = 'exercises';

            var tmpExercises = this.storage.getItem(this.storeageKey);
            if(tmpExercises) {
                ExercisesJson = tmpExercises;
                tmpExercises = null;
            }

            this.set($.parseJSON(ExercisesJson));
            WorkoutsJson = null;
        },
        checkServer: function () {
            var self = this;
            $.getJSON('http://openhouse.myda.ws/public/files/exercises.php?callback=?', function(exercises){
                if(exercises.length) {
                    self.storage.setItem(self.storeageKey, JSON.stringify(exercises));
                    self.set(exercises, {merge: true});
                }
            });
        }
    });

    return ExerciseCollection;
});