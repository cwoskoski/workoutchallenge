/**
 * Created by Mike on 2/26/14.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'models/workout',
    'text!../workouts.json'
], function ($, _, Backbone, WorkoutModel, WorkoutsJson) {
    var BottleCollection = Backbone.Collection.extend({
        model: WorkoutModel,
        initialize: function () {
            this.storage = window.localStorage;
            this.storeageKey = 'workouts';

            var tmpWorkouts = this.storage.getItem(this.storeageKey);
            if(tmpWorkouts) {
                WorkoutsJson = tmpWorkouts;
                tmpWorkouts = null;
            }

            this.set(this._workoutFilter($.parseJSON(WorkoutsJson)));
            WorkoutsJson = null;
        },
        checkServer: function () {
            var self = this;
            $.getJSON('http://openhouse.myda.ws/public/files/workouts.php?callback=?', function(workouts){
                if(workouts.length && JSON.stringify(self) != JSON.stringify(workouts)) {
                    console.log("Workout change detected");
                    Backbone.setAttribute('new-workouts', true);
                    workouts = self._workoutFilter(workouts);
                    self.storage.setItem(self.storeageKey, JSON.stringify(workouts));
                    self.set(workouts, {merge: true});
                } else {
                    console.log("Workouts did not change");
                }
            });
        },
        _workoutFilter: function(collection) {
            return _.filter(collection, function(wo){
                return (_.isObject(wo) && wo.name.substring(0,1) != '_');
            });
        }
    });

    return BottleCollection;
});