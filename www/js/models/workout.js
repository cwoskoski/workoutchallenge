/**
 * Created by Mike on 2/26/14.
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'gtvkeycontrol'
], function($, _, Backbone){
    var WorkoutModel = Backbone.Model.extend({

        exerciseCnt: 0,
        totalTime: 0,

        initialize: function(){
            this.calcSets();
        },

        calcSets: function(){
            var model = this;
            var setCnt = 0,
                exCnt = 0;
            $.each(this.get('sets'), function(ndxS, woSet){

                $.each(woSet.exercises, function(ndxE, exercise){
                    if(!exercise.excludeCount)
                    {
                        model.exerciseCnt++;
                    }

                    model.totalTime += exercise.time;
                });
            })
        },

        getEquipment: function(){
            var eArr = [];
            $.each(this.get('sets'), function(ndxS, woSet){

                $.each(woSet.exercises, function(ndxE, exercise){
                    eArr.push(exercise.equipment);
                });
            });

            return _.uniq(eArr);
        }
    });

    return WorkoutModel;
});