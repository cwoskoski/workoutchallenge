/**
 * Created by Mike on 2/26/14.
 */

define([
    'jquery',
    'underscore',
    'backbone'
], function($, _, Backbone){
    var ExerciseModel = Backbone.Model.extend({

        initialize: function(){

        }
    });

    return ExerciseModel;
});