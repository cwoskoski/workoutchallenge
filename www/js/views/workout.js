/**
 * Created by Mike on 2/26/14.
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'moment',
    'rotator',
    'countdown',
    'async',
    'bootbox',
    'moment',
    'text!../../templates/workout.html',
    'text!../../templates/speak.xml'
], function ($, _, Backbone, moment, Rotator, Countdown, async, bootbox, moment, WorkoutTemplate, SpeakTemplate) {
    var WorkoutView = Backbone.View.extend({
        el: $("#page"),
        imgRotator: false,
        bgImages: [],
        workout: false,
        sound: {},
        settings: {
            pictures: false,
            voice: false,
            queues: true
        },

        defaults: {
            restTime: 30,
            prepTime: 5
        },

        current: {
            set: 0,
            exercise: 0,
            count: -1,
            workoutTime: 0,

            total: 0,
            totalTime: 0
        },

        media: {},
        initialize: function (options) {

            var view = this;
            this.storage = window.localStorage;

            this.settings = Backbone.getAttribute('workout-settings', this.settings);

            view.workout = this.model.toJSON();

            console.log("Workout initializing");

            view.backCallbackId = null;
            view.timespan = null;
            view.rest = null;
            view.current.set = 0;
            view.current.exercise = 0;
            view.current.count = -1;
            view.current.workoutTime = 0;

            view.current.total = this.model.exerciseCnt;
            view.current.totalTime = this.model.totalTime;

            $(document).on('time-update', function (event, elapsed, total) {

                try {
                    if (elapsed && total) {
                        $(".main-countdown").html(moment.utc(1000 * (total - elapsed)).format("mm:ss"));

                        var middle = Math.ceil(total / 2);

                        if (total > 15 && elapsed == middle) {
                            view.playSound('half', 'Half way done', 2500);
                        }
                    }

                    $(".total-progress-bar .progress-bar").css('width', (view.current.workoutTime / view.current.totalTime * 100) + '%');

                    $(".total-progress-time").html(moment.utc(1000 * (view.current.totalTime - view.current.workoutTime)).format("HH:mm:ss"));
                } catch (e) {
                }
            });

            $(document).on('exercise-update', function (event, ex, nextEx) {

                try {
                    $(".total-progress-steps").html(Math.max(0, view.current.count) + " of " + view.current.total);

                    if (ex == 'get-ready') {
                        $(".current-exercise").html('Press play when you\'re<br/>ready to begin<br/><button class="btn btn-lg btn-default play"><i class="glyphicon glyphicon-play"></i></button>');
                        if (nextEx)
                            $(".next-exercise").html('Next: ' + nextEx.name);
                    } else if (ex == 'warmup') {
                        $(".current-exercise").html("Warm Up");
                        if (nextEx)
                            $(".next-exercise").html('Next: ' + nextEx.name);
                    } else if (ex == 'prep') {
                        if (nextEx)
                            $(".current-exercise").html('Next: ' + nextEx.name);
                        $(".next-exercise").html("");
                    } else if (ex == 'rest') {
                        $(".current-exercise").html('Rest');
                        if (nextEx)
                            $(".current-exercise").html('Next: ' + nextEx.name);
                    } else if (ex == 'done') {
                        $(".current-exercise").html('Finished');
                        $(".next-exercise").html("");
                    } else if (ex) {
                        $(".current-exercise").html(ex.name);
                        $(".current-exercise-icon > i").attr('class', 'icon icon-' + ex.equipment).show();
                        if (nextEx)
                            $(".next-exercise").html("Next: " + nextEx.name);
                    }
                } catch (e) {
                }
            });

            this.bgImages = [
                'http://runninginheels.co.uk/wp-content/uploads/2013/01/exercise-motivation.jpg',
                'http://2.bp.blogspot.com/-_wa0qj0-bAk/T2DU12CHvhI/AAAAAAAAAIQ/N3z8bObhmjY/s1600/Armstrong+Exercise+Motivation.png',
                'http://fitbodyhype.com/wp-content/uploads/2011/07/exercisemotivation7.jpg',
                'http://2.bp.blogspot.com/-SpLJEA3X8NY/Tz7Trpa5LsI/AAAAAAAABMs/HjgohubWm1w/s1600/Fitness+Motivation+Sign+25.jpg',
                'http://3.bp.blogspot.com/-LgvbP6woOlk/TnCi6Ug60BI/AAAAAAAAIQc/2-mlS6yeZAk/s1600/exer7.jpg',
                'http://fitbodyhype.com/wp-content/uploads/2011/07/exercisemotivation5.jpg',
                'http://www.contentfy.com/wp-content/uploads/2013/04/Exercise-Motivation-Tips-For-Beginners.jpg',
                'http://1.bp.blogspot.com/-5I3aQN_M7M0/T90X6gkMBQI/AAAAAAAAAgA/Ga5ZOVQDvY8/s1600/fitness-motivation.png',
                'http://fit4zen.files.wordpress.com/2013/03/yourfuture.jpg',
                'http://media-cache-ec0.pinimg.com/736x/cb/19/01/cb19017c4c76f6a8d003911e6103ce7e.jpg',
                'http://healthtoyourbody.net/wp-content/uploads/2013/02/motivational-quotes-3.jpg',
                'http://fitnesswordsblog.files.wordpress.com/2013/10/fc25a-3fitnessmotivationpictures.jpg',
                'http://healthflowint.com/wp-content/uploads/2013/12/fitness-motivation-wallpaper-hdwallpapers-fitness-motivation-to-exercise-page-sythe-org-forums-4bw2qbnn.jpg',
                'http://fitbodyhype.com/wp-content/uploads/2011/08/Dont-see-exercise-as-working-against-you.png',
                'http://www.chicagonow.com/body-by-bond/files/2013/03/the-rock-e1363022689660.jpg',
                'http://ts4.mm.bing.net/th?id=H.4727927006757602&pid=1.7',
                'http://3.bp.blogspot.com/-83dCK10KKdQ/UOHgBnvC3tI/AAAAAAAAO1E/RViVl5SYBk8/s1600/Exercise-Motivational-Photography-07.jpg',
                'http://recipeforabeautifullife.files.wordpress.com/2011/12/workout-motivation-8.jpg',
                'http://www.creatinganewmenow.com/wp-content/uploads/2012/09/fitness.jpg',
                'http://turboteacher.files.wordpress.com/2013/02/fitness-motivation-2_thumb.jpg?w=973&h=767',
                'http://fitnesswordsblog.files.wordpress.com/2013/10/fb869-4fitnessmotivationpictures.jpg',
                'http://stuffpoint.com/health-and-fitness-motivation/image/223757-health-and-fitness-motivation-motivation.jpg',
                'http://iambeast1.files.wordpress.com/2012/10/454.jpg',
                'http://2.bp.blogspot.com/-Mv8rp7Bh170/T-YvPG1KKPI/AAAAAAAAAHw/_D019SLTjEI/s1600/6.jpg',
                'http://www.creatingadestiny.com/wp-content/uploads/2012/03/Motivational-Fitness-Workout-Quotes-80.jpg',
                'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcT4dI63ML7SSrEMHcbAx53_3QPNHw1SMF9r6pAmSvSdEqBnXMj-',
                'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcR29ZhkdmRqV8kuaB-hPDgocADnS_iao2UCy0a-pgupdq4rSn_y0w',
                'http://8tracks.imgix.net/i/000/482/275/workout-motivation-pinterest-400x400-8402.jpg?q=65&sharp=15&vib=10&fm=jpg&fit=crop&w=521&h=521',
                'http://www.fitfabcities.com/wp-content/uploads/2012/06/Motivation-Monday-64.jpg',
                'http://fitsit360.com/wp-content/uploads/2013/05/Monday-Morning-Motivation-2.jpg',
                'http://www.marissarodriguez.com/blog/wp-content/uploads/2012/06/workout-motivation.jpg',
                'http://shechive.files.wordpress.com/2012/02/workout-motivation-20.jpg',
                'http://myfitstation.com/wp-content/uploads/2012/08/workout-motivation-1.jpg',
                'http://flattummyworkouts.com/wp-content/uploads/2012/02/flat-tummy-motivation-1.png',
                'http://fotoimagepics.info/wp-content/uploads/2013/12/workout_motivation_quotes_app.png',
                'http://media-cache-ec0.pinimg.com/736x/54/f0/5f/54f05f54d590f9e957622f4ef5774e86.jpg',
                'http://media-cache-ec0.pinimg.com/736x/db/55/4a/db554a8a1e21ec4ebeeb6bce71129c6f.jpg',
                'http://s2.favim.com/orig/34/motivation-quotes-tough-workout-Favim.com-277094.jpg',
                'http://media-cache-ec0.pinimg.com/736x/42/4a/44/424a443824d64f34fd1e768f328b3112.jpg',
                'http://media-cache-ak0.pinimg.com/736x/33/40/9e/33409e7481725e347a8034e3d1322dac.jpg',
                'http://media-cache-ak0.pinimg.com/736x/59/93/7b/59937b40c2d70d8fe1f1ef96c49b5f7c.jpg',
                'http://sixpackfactory.com/wp-content/uploads/2012/08/achive.jpg',
                'http://www.mytopvideos.com/wp-content/uploads/2013/06/best-workout-motivation-music-mix-2013-no-pain-no-gain.jpg',
                'http://doblelol.com/thumbs/workout-quotes-funny_4949650187486921.jpg'
            ];

            var dPresses = 0;
            $(document).keypress(function (e) {
                var code = e.keyCode || e.which;
                if (code == 100) { //d key
                    if (++dPresses % 4 == 0) {
                        $(".debug").fadeToggle();
                        dPresses = 0;
                    }
                }
            });

            window.headerView.hide();
            this.render();

            try {
                var voiceSettingsEl = $(".setting-voice"),
                    curLang;

                async.series([
                    function (cb) {
                        navigator.tts.startup(function (status) {
                            if(status == 1) {
                                cb(null, "startup:"+status);
                            } else if(status == 2) {
                                cb(null, "startup:"+status);
                            } else {
                                cb("Failed to startup: " + JSON.stringify(arguments));
                            }
                        }, function (status) {
                            cb("Failed to startup: " + JSON.stringify(arguments));
                        });
                    },
                    function (cb) {
                        navigator.tts.getLanguage(function (lang) {
                            curLang = 'en_US';

                            cb(null, "lang:"+lang);
                        }, function () {
                            cb("Failed to get current language: " + JSON.stringify(arguments));
                        });
                    },
                    function (cb) {
                        navigator.tts.setLanguage(curLang, function (status) {

                            cb(null, "setLang:"+status);
                        }, function () {
                            cb("Failed to get current language: " + JSON.stringify(arguments));
                        });
                    }
                ], function (err, args) {
                    if (err != null) {
                        if(window.isBeta)
                            Backbone.notification.alert("tts start error " + err);

                        voiceSettingsEl.addClass('no-tts');
                    } else {
                        if(window.isBeta)
                            Backbone.notification.alert("tts start successful: "+ JSON.stringify(args));

                    }
                });
            } catch (e) {
                if(window.isBeta)
                    Backbone.notification.alert("tts try/catch error " + e.message);

            }
        },

        events: {
            "click .setting": "editSettings",
            "click .play": "doPlay",
            "click .pause": "doPause",
            "click .restart": "doRestart",
            "click .page-back": "doBack"
        },

        onOpen: function () {
            var view = this;

            /*
             var zone1 = new gtv.jq.KeyBehaviorZone({
             containerSelector: '.workout-details',
             navSelectors: {
             item: '.item',
             itemParent: '.item-parent',
             itemRow: '.item-row'
             },
             selectionClasses: {
             basic: 'item-selected'
             },
             actions: {
             click: function(item){
             view.editSettings($(item));
             }
             },
             keyMapping: {
             13: function(selectedItem){
             view.editSettings($(selectedItem));
             return new gtv.jq.Selection('none');
             }
             }
             });

             var zone2 = new gtv.jq.KeyBehaviorZone({
             containerSelector: '.footer-details',
             navSelectors: {
             item: '.item',
             itemParent: '.item-parent',
             itemRow: '.item-row'
             },
             selectionClasses: {
             basic: 'item-selected'
             },
             actions: {
             click: function(item){
             var control = $(item).parent();
             control.trigger('click');
             }
             },
             keyMapping: {
             13: function(selectedItem, newSelected){
             var control = $(selectedItem).parent();
             control.trigger('click');

             if (newSelected) {
             return new gtv.jq.Selection('none');
             }

             var parentItems = selectedItem.parent().siblings();

             // Use jQuery to get the first item in the row.
             var replaceItem = parentItems.first().children();

             // Return this item, which the KeyController will now make the new selection.
             return new gtv.jq.Selection('selected', replaceItem);

             }
             }
             });

             this.keyController = new gtv.jq.KeyController();
             this.keyController.addBehaviorZone(zone1);
             this.keyController.addBehaviorZone(zone2);
             this.keyController.start(zone2, true);*/

            view.backCallbackId = _.registerGoBack(function (e) {
                setTimeout(function () {
                    view.doBack();
                }, 0);
                return false;
            });
        },

        onClose: function () {
            var view = this;
            console.log("workout:onClose:backId:" + view.backCallbackId);

            if (!_.isNull(view.backCallbackId)) {
                var unregistered = _.unregisterGoBack(view.backCallbackId);
                console.log("unregistered:" + unregistered);
            }

            $.each(this.sound, function (name, m) {
                try {
                    m.release();
                } catch (e) {
                }
            });

            try {
                window.plugins.insomnia.allowSleepAgain();
            } catch (e) {

            }

            try {
                window.plugin.notification.local.cancel(1, function () {
                    // The notification has been canceled
                });
            } catch (e) {
            }

            //this.keyController.stop();
            this.doPause();

            window.headerView.show();
        },

        render: function () {
            var view = this;

            var compiledTemplate = _.template(WorkoutTemplate, {workout: view.workout});
            this.$el.html(compiledTemplate);

            if (view.imgRotator) {
                view.imgRotator.stop();
                view.imgRotator = null;
            }

            view.imgRotator = new Rotator(view.$el.find(".screensaver > .panning > img"), view.bgImages, 30000, function () {
                console.log('image changed');
            });

            $(".current-workout").html(view.workout.name);
            $(".total-time").html('');
            $(document).trigger('time-update', []);
            $(document).trigger('exercise-update', ['get-ready']);

            var debug = $(".debug");
            debug.find(".screen").html($(window).width() + 'x' + $(window).height());

            $.each(view.settings, function (key, val) {
                if (val)
                    $(".settings-control[data-setting='" + key + "']").addClass('active');
            });

            try {
                window.plugins.insomnia.keepAwake();
            } catch (e) {

            }
        },

        editSettings: function (event) {
            var settingItem = $(event.currentTarget);
            console.log(settingItem);

            var view = this;
            var parent = settingItem.parent();
            parent.toggleClass('active');

            view.settings[parent.data('setting')] = parent.hasClass('active');
            Backbone.setAttribute('workout-settings', view.settings);

            if (settingItem.hasClass('setting-pictures')) {
                if (parent.hasClass('active')) {
                    view.imgRotator.start();
                }
                else {
                    view.imgRotator.stop();
                }
            } else if (settingItem.hasClass('setting-voice')) {
                if (settingItem.hasClass('no-tts')) {
                    Backbone.notification.confirm("It appears you don't have Text To Speech installed on your device, would you like to get details on how fix this issue?", function (btnNdx) {
                        console.log('no tts:' + btnNdx);
                        if (btnNdx == 1) {
                            Backbone.history.navigate("tts", true);
                        }
                    }, null, ["Ok", "Cancel"]);
                }
            }
        },

        doBack: function (event) {
            if (event)
                event.preventDefault();

            if (this.timespan && !this.timespan.isPaused() && this.timespan.getRemainingTime() != 0) {

                Backbone.notification.confirm("A workout is currently active, are you sure want to stop this workout?", function (btnNdx) {
                    console.log("confirm: " + JSON.stringify(arguments));
                    if (btnNdx == 1) {
                        window.history.back();
                    }
                });
            } else {
                window.history.back();
            }

            return false;
        },

        doPlay: function () {

            var view = this;

            if (view.timespan && view.timespan.getRemainingTime() && view.timespan.isPaused()) {
                view.timespan.resume();
            }

            if (view.prep && view.prep.getRemainingTime() && view.prep.isPaused()) {
                view.prep.resume();
            }

            if (view.rest && view.rest.getRemainingTime() && view.rest.isPaused()) {
                view.rest.resume();
            }

            try {
                window.plugin.notification.local.add({
                    id: 1,
                    message: view.workout.name + " is currently running.",
                    icon: 'ic_launcher',
                    sound: null,
                    ongoing: true
                });
            } catch (e) {
            }

            if (view.current.count == -1) {
                view.current.count++;
                var set = view.workout.sets[view.current.set];
                var ex = set.exercises[view.current.exercise];

                //console.log(set, ex);

                view.tts('Start ' + ex.name);
                view.logWorkout('start');
                view.doNext();
            }

            $(".workout").removeClass('stopped').removeClass('ready').addClass('working');
        },

        doPause: function () {

            console.log("Exercise paused");

            var view = this;

            if (view.timespan && view.timespan.getRemainingTime() && !view.timespan.isPaused()) {
                view.timespan.pause();
            }

            if (view.prep && view.prep.getRemainingTime() && !view.prep.isPaused()) {
                view.prep.pause();
            }

            if (view.rest && view.rest.getRemainingTime() && !view.rest.isPaused()) {
                view.rest.pause();
            }

            $(".workout").removeClass('working').addClass('stopped');

            try {
                window.plugin.notification.local.cancel(1, function () {
                    // The notification has been canceled
                });
            } catch (e) {
            }
        },

        doRestart: function () {
            document.location = document.location;
        },

        doNext: function () {

            var view = this;

            var set = view.workout.sets[view.current.set];
            var nextSet = (view.current.set < view.workout.sets.length - 1) ? view.workout.sets[view.current.set + 1] : false;
            var ex = set.exercises[view.current.exercise];

            var nextEx = false;
            if (view.current.exercise < set.exercises.length - 1) {
                nextEx = set.exercises[view.current.exercise + 1]
            } else if (nextSet) {
                nextEx = nextSet.exercises[0]
            }

            view.current.count++;

            var restTime = set.restTime || view.defaults.restTime;

            $(document).trigger('exercise-update', [ex, ((set.restTime < 0 || view.current.exercise < set.exercises.length - 1) ? nextEx : {name: 'Rest'})]);
            $(document).trigger('time-update', [0, ex.time]);

            $(".workout").addClass('working');

            //var duration = 30;
            var duration = ex.time;

            view.timespan = Countdown(duration, function (remaining) {
                console.log(remaining);

                if (ex.time != remaining)
                    view.current.workoutTime++;
                $(document).trigger('time-update', [(ex.time - remaining), ex.time]);

                if (remaining == 5 && view.current.exercise < set.exercises.length - 1) {
                    view.tts("Next up: " + nextEx.name);
                }

            }, function () {

                view.current.workoutTime++;
                $(document).trigger('time-update', [ex.time, ex.time]);
                view.playSound('stop', 'Exercise complete', 2500);

                $(".workout").removeClass('working');

                if (view.current.exercise < set.exercises.length - 1) {
                    var prepTime = (_.isUndefined(nextEx.prepTime)) ? view.defaults.prepTime : parseInt(nextEx.prepTime);

                    $(document).trigger('exercise-update', ['prep', nextEx]);
                    view.current.exercise++;

                    $(".workout").addClass('prepping');

                    // Prepping
                    view.prep = Countdown(
                        prepTime,
                        function (remaining) {
                            $(document).trigger('time-update', [(prepTime - remaining), prepTime]);
                        }, function () {
                            $(document).trigger('time-update', [prepTime, prepTime]);
                            $(".workout").removeClass('prepping');
                            view.doNext();
                        }
                    );

                } else if (nextSet) {
                    $(document).trigger('exercise-update', ['rest', nextEx]);

                    view.current.set++;
                    view.current.exercise = 0;

                    // Resting
                    var restTime = set.restTime || view.defaults.restTime;

                    if (restTime > 0) {
                        $(".workout").addClass('resting');
                        view.tts('Rest');
                    }

                    view.rest = Countdown(
                        restTime,
                        function (remaining) {
                            $(document).trigger('time-update', [(restTime - remaining), restTime]);

                            if (nextEx) {
                                if (remaining == 7) {
                                    view.tts("Next up: " + nextEx.name);
                                } else if (remaining == 3) {
                                    view.playSound('start', 'Get ready to start', 3500);
                                }
                            }


                        }, function () {
                            $(document).trigger('time-update', [restTime, restTime]);
                            $(".workout").removeClass('resting');
                            view.doNext();
                        }
                    );
                } else {
                    $(document).trigger('exercise-update', ['done']);
                    try {
                        window.plugins.insomnia.allowSleepAgain();
                    } catch (e) {

                    }
                    view.logWorkout('finished');
                    view.tts("Finished: Great Job!");
                    view.doPause();
                }
            });
        },

        playSound: function (name, text, duration) {

            var view = this;

            try {

                if (!$(".setting-queues").parent().hasClass('active')) {
                    console.log("Play Media OFF: " + name);
                    return;
                }

                console.log("Play Media ON: " + name);

                var package_name = 'com.cdubenterprises.workoutchallenge.beta';

                window.plugin.notification.local.add({
                    id: 2,
                    icon: 'ic_launcher',
                    message: text,
                    sound: 'android.resource://' + package_name + '/raw/' + name
                });

                setTimeout(function () {
                    window.plugin.notification.local.cancel(2, function () {
                        // The notification has been canceled
                    });
                }, (duration || 2500));
                return;

                if (!view.sound[name]) {
                    console.log("loading media: " + name);
                    view.sound[name] = new Media(view._assetPath("media/" + name + ".mp3"), function () {
                        console.log("media success");
                    }, function (error) {
                        console.log('media failed: ' + error.message);
                    }, function (status) {
                        console.log("status: " + status);

                        if (status == Media.MEDIA_STOPPED) {

                        }
                    });
                    view.sound[name].play();

                    /*var loadedInterval;
                     loadedInterval = setInterval(function(){
                     if(loaded) {
                     view.sound[name].play();
                     clearInterval(loadedInterval);
                     }
                     }, 50);*/
                }
                else {
                    view.sound[name].play();
                }

            } catch (e) {

                try {
                    view.sound[name] = new Audio();
                    view.sound[name].src = 'media/' + name + '.mp3';
                    view.sound[name].addEventListener('ended', function () {
                        console.log("Audio done playing");
                        /*if(cb)
                         cb();*/
                    });
                    view.sound[name].play();
                } catch (e) {
                    console.log("Play Media ERROR: %s", e.message);
                }
            }
        },

        tts: function (str) {
            try {

                if (!$(".setting-voice").parent().hasClass('active')) {
                    console.log("Play TTS OFF: %s", str);
                    return;
                }

                console.log("Play TTS ON: [" + str + "], navigator.tts: %s, chrome.tts: %s", $.type(navigator.tts), ('speechSynthesis' in window));

                if ('speechSynthesis' in window) {
                    var ssml = _.template(SpeakTemplate, {sentences: str.split(":")});
                    console.log("SSML:\r\n%s", ssml);
                    var u = new SpeechSynthesisUtterance(ssml);
                    speechSynthesis.speak(u);
                }
                else {
                    window.plugin.audioFocus.request();
                    var parts = str.split(":");

                    async.mapSeries(parts, function (newStr, sCb) {
                        navigator.tts.speak(newStr, function () {
                            console.log('tts success [' + newStr + ']');
                            setTimeout(function () {
                                sCb();
                            }, 100);
                        }, function (error) {
                            console.log('tts failed: ' + JSON.stringify(arguments));

                            if(window.isBeta)
                                Backbone.notification.alert("tts speak error " + JSON.stringify(arguments));

                            sCb(error);
                        });
                    }, function (err, result) {
                        console.log('tts done releasing');
                        window.plugin.audioFocus.release();
                    });
                }
            } catch (e) {
                console.log("TSS Error: " + e.message);

                if(window.isBeta)
                    Backbone.notification.alert("tts speak error " + e.message);
            }
        },

        _assetPath: function (append, filePart) {

            var path = window.location.pathname;
            path = path.substr(path, path.length - 10);
            return (filePart === false ? '' : 'file://') + path + append;

        },

        logWorkout: function (action, data) {
            var view = this;
            var logs = view.storage.getItem('logs');

            if (logs)
                logs = $.parseJSON(logs);
            else
                logs = [];

            logs.push({
                wo: view.workout.name,
                time: moment().format(),
                action: action,
                data: data
            });

            view.storage.setItem('logs', JSON.stringify(logs));
        }
    });

    return WorkoutView;
});