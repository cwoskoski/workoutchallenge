/**
 * Created by Mike on 10/31/2014.
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'moment',
    'async'
], function($, _, Backbone, moment, async){
    var HeaderView = Backbone.View.extend({
        el: $("#header"),
        initialize: function(options){

            this.render();
        },
        events: {
            "click .back": "doBack"
        },
        onOpen: function() {

        },

        onClose: function() {

        },
        render: function(){
            //this.$el = $(".header");
        },
        hide: function(){
            this.$el.addClass('hidden');
        },
        show: function(){
            this.$el.removeClass('hidden');
        },
        setActive: function(optionCls, title) {
            var option = this.$el.find("."+optionCls);

            if(!option.hasClass('active')) {
                this.$el.find(".option").removeClass('active');

                setTimeout(function () {
                    option.find(".header-title").html(title || '');
                    option.addClass('active');
                }, 200);
            } else {
                option.find(".header-title").html(title || '');
            }
        },
        doBack: function(event){
            console.log('back');

            if(event)
                event.preventDefault();

            window.history.back();

            return false;
        }
    });

    return HeaderView;
});