/**
 * Created by Mike on 2/26/14.
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'moment',
    'rotator',
    'countdown',
    'async',
    'bootbox',
    'moment',
    'views/workout_list',
    'views/workout',
    'views/workout_details'
], function($, _, Backbone, moment, Rotator, Countdown, async, bootbox, moment, WorkoutListView, WorkoutView, WorkoutDetails){
    var PreviewView = Backbone.View.extend({
        el: $("#page"),
        workout: false,
        sound: {},
        settings: {
            pictures: false,
            voice: false,
            queues: true
        },

        defaults: {
            restTime: 30,
            prepTime: 5
        },

        current: {
            set: 0,
            exercise: 0,
            count: -1,
            workoutTime: 0,

            total: 0,
            totalTime: 0
        },

        media: {},
        initialize: function(options){

            var view = this;
            this.storage = window.localStorage;

            view.previewNdx = 0;
            view.previews = [
                {
                    message: "Click anywhere on the workout to either view the workout details or to do the workout",
                    element: ".item-actions-indicator:first .glyphicon-chevron-down",
                    page: WorkoutListView,
                    pageArgs: {collection: options.collection},
                    offsetY: -40,
                    offsetX: 0

                },
                {
                    message: "Click the order options to change how the list of workouts is organized",
                    element: ".workout-display-options",
                    page: WorkoutListView,
                    pageArgs: {collection: options.collection},
                    offsetY: 0,
                    offsetX: 0
                },
                {
                    message: "Click on a exercise for a instruction on how to perform the exercise",
                    element: ".item-actions-indicator .glyphicon-chevron-down",
                    page: WorkoutDetails,
                    pageArgs: {model: options.model},
                    offsetY: -30,
                    offsetX: 0
                },
                {
                    message: "Symbol indicates what equipment is needed for the exercise",
                    element: ".exercise-icon:first",
                    page: WorkoutDetails,
                    pageArgs: {model: options.model},
                    offsetY: 0,
                    offsetX: 15
                },
                {
                    message: "Click the headphone icon to enable / disable voice commands",
                    element: ".setting-voice",
                    page: WorkoutView,
                    pageArgs: {model: options.model},
                    offsetY: 0,
                    offsetX: 10
                },
                {
                    message: "Click the speaker icon to enable / disable sound cues",
                    element: ".setting-queues",
                    page: WorkoutView,
                    pageArgs: {model: options.model},
                    offsetY: 0,
                    offsetX: 10
                }
            ];

            setTimeout(function(){
                view.render();
            }, 10);

        },

        onOpen: function() {
            var view = this;

            console.log('preview.onOpen');

            view.overlay = $('<div class="preview"></div>');
            view.bubble = $('<div class="bubble"><div class="bubble-container"><div class="arrow"></div><div class="bubble-content"></div><div class="bubble-next"><button class="btn btn-default btn-lg btn-block next">Got it</button></div></div></div>');
            view.overlay.append(view.bubble);
            $("body").append(view.overlay);
        },

        onClose: function() {
            var view = this;
            $(".preview").remove();
        },

        render: function(event){
            var view = this;

            //this.$el.html(compiledTemplate);

            if(event)
                view.previewNdx++;

            var windowWidth,
                page,
                preview,
                element,
                offsetY = 30,
                offsetX = 20,
                bubbleWidth,
                elWidth,
                elOffset,
                positionX,
                positionY;

            if(view.previewNdx >= view.previews.length)
            {
                Backbone.setAttribute('previewed', true);
                //Backbone.history.navigate("", {trigger: true});
                window.location.reload();
                //Backbone.history.loadUrl("");
                return;
            }

            preview = view.previews[view.previewNdx];

            page = new preview.page(preview.pageArgs);
            windowWidth = $(window).width();

            view.bubble.find('.bubble-content').html(preview.message);
            element = view.$el.find(preview.element);

            bubbleWidth = view.bubble.width();
            elWidth = element.width();
            elOffset = element.offset();

            console.log("right side:" + (elOffset.left > (windowWidth / 2)));

            if(elOffset.left > (windowWidth / 2)) {
                view.bubble.removeClass('bubble-left').addClass('bubble-right');

                view.bubble.css('max-width', elOffset.left - 30);
                bubbleWidth = view.bubble.width();

                positionX = elOffset.left - (bubbleWidth + offsetX + 20) + preview.offsetX;
                console.log("right");
            } else {
                view.bubble.removeClass('bubble-right').addClass('bubble-left');
                positionX = elOffset.left + offsetX + preview.offsetX;

                view.bubble.css('max-width', windowWidth - positionX - 30);
                bubbleWidth = view.bubble.width();

                console.log("left");
            }

            positionY = elOffset.top + offsetY + preview.offsetY;

            console.log("window: "+ windowWidth +", el.left: "+ elOffset.left +", el.width: "+elWidth +", posX: "+ positionX +", posY:" + positionY);

            view.bubble.animate({
                left: positionX + 'px',
                top: positionY + 'px'
            }, 500);

            view.bubble.find(".next").one('click', function(event){
                view.render(event);
            });

        }
    });

    return PreviewView;
});