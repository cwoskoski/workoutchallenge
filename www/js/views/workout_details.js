/**
 * Created by Mike on 2/26/14.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'moment',

    'text!../../templates/workout_details.html'
], function($, _, Backbone, moment, WorkoutDetailsTemplate){
    var WorkoutListView = Backbone.View.extend({
        el: $("#page"),
        initialize: function(options){

            this.storage = window.localStorage;
            this.workout = this.model;
            this.exercises = options.exercises;

            this.render();
        },
        events: {
            "click .workout-item": "selectWorkout",
            "click .workout-details-item": "showDescription"
        },
        onOpen: function() {
            window.headerView.setActive('back', 'Details - '+ this.workout.get('name'));
        },

        onClose: function() {

        },
        render: function(){
            var view = this;

            console.log(view.workout);
            var compiledTemplate = _.template(WorkoutDetailsTemplate, {workout: view.workout});
            this.$el.html(compiledTemplate);
        },
        selectWorkout: function(event){
            var el = $(event.currentTarget);
            Backbone.history.navigate("workout/"+ el.data('name'), true);
        },
        showDescription: function(event){
            var itm = $(event.currentTarget)
                descContainer = itm.find(".exercise-description");

            var ex = this.exercises.findWhere({name: itm.find(".exercise-name").text()});

            if(!ex){
                return;
            }

            descContainer.text(ex.get('description')).toggleClass('open');
            setTimeout(function(){
                itm.find('.item-actions-indicator > i').toggleClass('open');
            }, 100);
        }
    });

    return WorkoutListView;
});