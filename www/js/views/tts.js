/**
 * Created by Mike on 2/26/14.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'moment',
    'async',

    'text!../../templates/tts.html',
    'text!../../templates/speak.xml'
], function($, _, Backbone, moment, async, WorkoutDetailsTemplate, SpeakTemplate){
    var TtsView = Backbone.View.extend({
        el: $("#page"),
        initialize: function(options){

            this.render();

            try {
                navigator.tts.startup(function () {
                    console.log("tts started successfully: "+ JSON.stringify(arguments));
                }, function () {
                    console.log("tts failed to start: "+ JSON.stringify(arguments));
                });
            } catch (e) {
                console.log("tts start error "+ e.message);
            }
        },
        events: {
            "click .back-button": "doBack",
            "click .test-tts": "doText"
        },
        onOpen: function() {
            window.headerView.setActive('back', 'Text-to-Speech Guide');
        },

        onClose: function() {

        },
        render: function(){
            var view = this;

            console.log(view.workout);
            var compiledTemplate = _.template(WorkoutDetailsTemplate, {});
            this.$el.html(compiledTemplate);
        },
        doBack: function(event){
            if(event)
                event.preventDefault();

            window.history.back();

            return false;
        },
        doText: function(){
            var str = "Workout Challenge knows how to talk!";

            if('speechSynthesis' in window)
            {
                var ssml = _.template(SpeakTemplate, {sentences: str.split(":")});
                console.log("SSML:\r\n%s", ssml);
                var u = new SpeechSynthesisUtterance(ssml);
                speechSynthesis.speak(u);
            }
            else
            {
                window.plugin.audioFocus.request();
                var parts = str.split(":");

                async.mapSeries(parts, function(newStr, sCb){
                    navigator.tts.speak(newStr, function () {
                        console.log('tts success ['+newStr+']');
                        setTimeout(function(){
                            sCb();
                        }, 100);
                    }, function (error) {
                        console.log('tts failed: ' + error.message);
                        sCb(error);
                    });
                }, function(err, result){
                    console.log('tts done releasing');
                    window.plugin.audioFocus.release();
                });
            }
        }
    });

    return TtsView;
});