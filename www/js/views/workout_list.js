/**
 * Created by Mike on 2/26/14.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'moment',
    'text!../../templates/workout_list.html',
    'text!../../templates/_workout_item.html'
], function($, _, Backbone, moment, WorkoutListTemplate, WorkoutItemTemplate){
    var WorkoutListView = Backbone.View.extend({
        el: $("#page"),
        initialize: function(){

            this.displayOption = Backbone.getAttribute('workout-list-order', 'alphabetical');

            this.render();
            this.listenTo(this.collection, 'all', function(){
                this.render();
            });
        },
        events: {
            "click .workout-item": "showActions",
            "click .workout-display-options button": "changeDisplay",
            "click .do-workout": "selectWorkout",
            "click .view-details": "viewDetails"
        },
        onOpen: function() {
            var view = this;
            var zone1 = new gtv.jq.KeyBehaviorZone({
                containerSelector: '.header',
                navSelectors: {
                    item: '.item',
                    itemParent: '.item-parent',
                    itemRow: '.item-row'
                },
                selectionClasses: {
                    basic: 'item-selected'
                },
                actions: {
                    click: function(item){
                        view.editSettings($(item));
                    }
                },
                keyMapping: {
                    13: function(selectedItem){
                        view.editSettings($(selectedItem));
                        return new gtv.jq.Selection('none');
                    }
                }
            });

            this.keyController = new gtv.jq.KeyController();
            this.keyController.addBehaviorZone(zone1);
            this.keyController.start(zone1, true);

            window.headerView.setActive('menu', 'Workout Challenge');
        },

        onClose: function() {
            this.keyController.stop();
        },

        render: function(){
            var view = this;

            var compiledTemplate = _.template(WorkoutListTemplate, {itemTpl: WorkoutItemTemplate, workouts: this.collection, display: this.displayOption, newWorkouts:  Backbone.getAttribute('new-workouts')});
            Backbone.setAttribute('new-workouts', false);
            this.$el.html(compiledTemplate);
        },
        showActions: function(event){
            var itm = $(event.currentTarget);

            itm.find('.item-actions').toggleClass('open');
            setTimeout(function(){
                itm.find('.item-actions-indicator > i').toggleClass('open');
            }, 100);

            /*$(".workout-item.open").find('.item-actions').slideToggle(100, function(){
                $(this).parents(".workout-item").toggleClass('open').find('.item-actions-indicator > i').toggleClass('open');
            });*/

            /*itm.find('.item-actions').slideToggle(100, function(){
                itm.toggleClass('open').find('.item-actions-indicator > i').toggleClass('open');
            });*/
        },
        selectWorkout: function(event){
            event.stopPropagation();
        },
        checkForUpdate: function(){
            this.collection.checkServer();
        },
        changeDisplay: function(event) {
            var option = $(event.currentTarget).data('option');

            if(option == this.displayOption)
                return;

            console.log("Changing to display: %s", option);
            Backbone.setAttribute('workout-list-order', option);
            this.displayOption = option;

            this.render();

        },
        viewDetails: function(event){
            event.stopPropagation();
        }
    });

    return WorkoutListView;
});